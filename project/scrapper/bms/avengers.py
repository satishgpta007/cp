#!/usr/local/bin/python

import requests
import pync

from bs4 import BeautifulSoup

FAV_VENUES = ["PVR:VegaCity,BannerghattaRoad", "PVR:ForumMall,Koramangala"]
BMS_URL = "https://in.bookmyshow.com/buytickets/avengers-endgame-bengaluru/movie-bang-ET00100668-MT/20190501"

response = requests.get(BMS_URL, allow_redirects=False, timeout = 5)
print response,BMS_URL

content = BeautifulSoup(response.content, "html.parser")

# print content
names = content.findAll('a', attrs={"class": "__venue-name"})


def sendSMS(message):
            MessageUrl = "https://www.fast2sms.com/dev/bulk"
            payload = "sender_id=FSTSMS&message={}&language=english&route=p&numbers=8721876940,9402591252,8174945133".format(message)
            headers = {'authorization': "kMtXlsGWRJ8fAwbxa9NmUeuDTLgEr0nYIK1OpQoZdy65F4ViPq1i8QDF9vpuU7WhdmcxIkbw0RjfPtCl",'Content-Type': "application/x-www-form-urlencoded",'Cache-Control': "no-cache",}
            responseForMessageSent = requests.request("POST", MessageUrl, data=payload, headers=headers)
            print (responseForMessageSent.text)

for name in names:
            venueName = name.find('strong').text
            venueName = "".join(venueName.split())
            
            if venueName in FAV_VENUES:
                        toNotify = "ticket booking started for {} center.".format(venueName)
                        pync.notify(toNotify)
                        sendSMS(toNotify)
            

# TODO: Correct the date. 
# Add mobile notifications features
# Test the sms features
