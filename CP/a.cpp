#include "bits/stdc++.h"
using namespace std;
 
#define chota(a) transform(a.begin(), a.end(), a.begin(), ::tolower)
#define pure cout << fixed << setprecision(18)
#define kitnabit(x) __builtin_popcount(x)
#define ll long long
#define newline cout << "\n"

const int N = 1e5 + 2;
int n;
int m;
vector<pair<int, int> > g[N];
int dist[N];
priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > pq;

int main() {
    ios_base::sync_with_stdio(0);
    cin >> n >> m;
    for (int i = 1; i <= m; i++) {
        int x, y, c;
        cin >> x >> y >> c;
        g[x].push_back({y, c});
        g[y].push_back({x, c});
    }
    for (int i = 1; i <= n; i++) dist[i] = INT_MAX;
    pq.push({0, 1});
    dist[1] = 0;
    while (!pq.empty()) {
        int u = pq.top().second;
        int cost = pq.top().first;
        pq.pop();
        if (cost <= dist[u]) {
            for (auto it : g[u]) {
                int v = it.first;
                int toCost = it.second;
                if (cost + toCost < dist[v]) {
                    dist[v] = cost + toCost;
                    pq.push({dist[v], v});
                }
            }
        }
    }
    for (int i = 1; i <= n; i++) cout << dist[i] << " ";
    newline;
}